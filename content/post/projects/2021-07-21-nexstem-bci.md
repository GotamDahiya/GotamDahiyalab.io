---
title: Eyeblinks and Jaw Clenches
subtitle: Predicting Eye Blinks and Jaw Clecnches using EEG values
link_title: BCI Assignment
link_project: https://gitlab.com/GotamDahiya/nexstem-assignment
date: 2021-07-21 12:50:00
tags: ["deep learning", "bci", "classification"]
summary: 
    This project was undertaken as a shortlist assignment by the company NexStem. I had to predict whether certain EEG values corresponded to an Eye Blink or a Jaw Clench.

toc: true
weight: "4"
---

## Introduction

This project aims at classifying a certain time period of EEG readings. The readings can either correlate to an Eye Blink or a Jaw Clench. The EEG recordings are stored in a CSV file along with the time stamp of when they were collected. This is similar to what Neural Link is working on.

There are multiple ways to approach this classification. 
1. One way is to use simple regression models such as Logistic Regression or Support Vector Machines or Decision Trees.
2. Using unsupervised methods such as K-Means, Gaussian Mixture or DB Scan.
3. Using Artificial Neural Networks(ANNs) to classify the readings. Eg : Dense layers
4. Using Convolutional Neural Networks(CNNs) to classify subsets of the data in one go. Eg : Conv2D layers

Neural network algorithms will work much better than the standard machine learning algorithms as there may not be any linear relationships between the readings and the actual output. I chose to go with ANNs for classifying one set of readings rather than the CNNs approach of classifying a group of set of recordings.

## Dataset Expansion and Cleaning

### Expansion

A CSV file was given with 46066 rows and 19 columns. 14 columns denoted the different EEG readings, first column for the Timestamp of the recorded reading, *MarkerValueInt* denoting which type of reading and three other columns which were dropped. The empty values in the *MarkerValueInt* column were substituted with *-100* for visualising the distribution of readings.

A time period of 0.5 seconds was given for each reading recorded. 0.2 seconds before and 0.3 seconds after. The value was updated only if the present reading was *-100* or *NaN*. The check was done so that no recorded reading is changed by the expansion.

### Cleaning

The column *MarkerIndex* was dropped as it had no value to the recordings. The remaining *-100* values were replaced with 0 to indicate no reading. The *Timestamp* column was kept but not included when the data was used for training the model. It was used when the model was being tested on unseen data.

### Dataset Splitting

The dataset was split into 3 sets, training, validation and testing in the ratio of 70%, 20% and 10%. The model was trained on the training set and accuracy was measured using the validation set. The testing set was used to see how the model worked on unseen data. The results of the unseen data were taken into account whether the model was good or not.

## Model Creation

### Preprocessing

<!-- As there were only three classes, Nothing(0.0), Eye Blink(22.0) and Jaw Clench(23.0)  -->
The three classes, Eye Blink(22.0), Jaw Clench(23.0) and Nothing(0.0) were one-hot encoded so that the model does not give Jaw Clench a higher priority due to its greater label value. 

The EEG recordings were scaled using Standard Scaling which approximates the data to a bell curve. The data is approximated to a normal function. To be honest, I could have used Min-Max Scaling, Logarthmic normalisation or L2 Normalisation(good for comparing the features vectors). The scaling/normalisation depends upon the raw data.

### Model Defining

4 Dense layers were used with the final layer giving the output. ```ReLU``` Activation is used for the first three layers while ```Softmax``` is used for the final layers to create the output probability logits.

BatchNormalisation was applied to the results of the first three layers after the activation function. Following this ```Dropout``` with *0.25* rate is used to make the model more robust to external noise. In BCI interfaces, external and internal noise is a major drawback. Models have to counter this by either roboust or the preprocessing steps remove a majority of the noise.

### Learning Rate

A cyclic learning rate was used instead of a constant or step decreasing learning rate. This allowed the model to pop out of a local minimium if got stuck inside of it. There are three versions ```triangular, triangular2 and exponential```.The frequency of the oscillations is controlled by the stepsize. 

- Triangular has a constant maximum and minimum learning rate.
-  Triangular2 has a constant minimum learning rate, but a gradually decreasing maximum learning rate.
- Exponential has a constant minimum learning rate and a exponentially decreasing maximum learning rate.

```Triangular2``` and ```Exponential``` have a better chance of finding and remaining in the global minimum due to the decreaing maximum learning rate, while ```Triangular``` can hop out of the global minimum due to the constant maximum learning rate.

The original paper explaining these learning rates can be found at [Cyclical Learning Rates for Training Neural Networks](https://arxiv.org/abs/1506.01186v6). Step size is the number of iterations in which the learning rate cycles between minimum and maximum learning rate. As per the authour of the above paper the step size should be 2 to 10 times the total number of iterations.

### Model Training

The model was trained for 200 epochs with a batch size of 16. These two hyperparameters were decided after multiple training iterations of the model. ```Triangular``` cyclic learning mode was chosen with the following parameters : 

| Parameter | Value |
| --------- | ----- |
| Minimum Learning rate | 10<sup>-4</sup> |
| Maximum Learning rate | 10<sup>-3</sup> |
| Step Size | 2000. |
| gamma | 1. | 

The weights corresponding to the maximum validation accuracy were stored in a H5 file. The model was also saved to an H5 file.

A confusion matrix was made to check how the model classified the validation data. *Nothing* had the highest score followed by *Eye Blink* and *Jaw Clench*.

## Prediction

A time period of 0.5 seconds with a step-size of 0.5 seconds is taken for classifying a certain action. Depending upon