---
title: Land Usage Classification using Sentinel 2 Data
date: 2021-07-12
tags: ["paper", "thesis", "deep learning", "cnns", "gis"]
summary: 
    The aim of this thesis is to classify land usage into 10 classes, Residential, Industrial, Highway, Pasture, Permanent Crop, River, Sea & Lake, Herbaceous Vegetation, Forest, Annual Crop, using Sentinel 2 Level 1C imagery obtained from [Copernicus Hub](https://scihub.copernicus.eu). 

    The model developed was trained on the EuroSat dataset.
link_title: Hello World
toc: true
weight: "1"
---

<!-- # Table of Contents
* [Dataset](#dataset)
* [Model](#model)
* [Pre-and-Post-Processing](#processing) -->


## Dataset

The [Eurosat dataset](https://github.com/phelber/EuroSAT) is used for training the model developed for classifying the land usage classes. It consists of 10 classes, Residential, Industrial, Highway, Pasture, Permanent Crop, River, Sea & Lake, Herbaceous Vegetation, Forest, Annual Crop each with 2000 to 3000 images. New images were added to the original Sea & Lake dataset to counter the misclassification occuring at 21km of the shoreline. [European Urban Atlas 2012 edition](https://land.copernicus.eu/local/urban-atlas/urban-atlas-2012) was used as the ground truth data while making the original dataset.

The dataset only contains Sentinel 2 Level 1C images which are Top of Atmoshpere[TOA] Reflectance. The atmoshpere does distort the actual values present on the surface.

## Model

A SqueezeNet architecture was adopted for the model put into production. One major point of selection of this model architecture was the reduced number of parameters *reducing* RAM and CPU usages as well as a more diversified training due to the splitting of one layer into 2 and adding their feature maps.

## Pre-and-Post-Processing

L2 normalisation was applied to each of the 6 bands used for classifying passed to the model. This allows the model to make a easier comparison between the arrays and increases the classification accuracy. The [Copernicus Global Land Cover Layers - Collection 2](https://www.mdpi.com/2072-4292/12/6/1044)[`LULC 100m ESA`] was used as check if the model was confused between two classes or the classification accuracy was too low. 

A higher level of class nomenclature was introduced which was used for,
1. Checking with the LULC 100m ESA dataset.
2. If the highest probability was less than 35%.

The LULC 100m ESA dataset was used when,
1. Highest probability is less than 45% and the difference between highest and second highest is less than 10%.
2. Highest probability is less than 20%.

In case the super class predicted from the model does not match with the super class from the LULC 100m ESA dataset, then the LULC 100m ESA datset is taken.