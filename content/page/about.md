---
title: About me
subtitle: Get to know me
comments: false
---

My name is Gotam Dahiya. My nicknames inlcude *Maverick* and *Orion*.

I am currently working as Machine Learning Engineer specialising in Computer Vision at KawaSpace. I am currently part of the Research and Development Team. I deal with GIS stuff, like finding farm boundaries, detecting buildings, classification of land cover and so on. My unofficial designation is *Applied Science SWAT Team Lead*.

I like creating stuff and have an interest in robotics. This stems from my childhood interest in mechanics and cars, teenage interest in electronics and physics and finally college interest in coding. I also play squash, badminton and football. I enjoy swimming and horse-riding.

Astronomy is one of my hobbies. Any chance I get and the telescope is outside swinging around trying to get a glimpse of the Orion Nebula or Jupiter or Saturn. I like to watch thriller, mystery, war, action and comedy movies. The same applies to books which also includes Commando Comics series and Deadpool comic series.  

One interesting hobby I have taken up is creating Singleplayer and Multiplayer missions in Arma 3. I am also thinking of taking up modding.

I also plan on exploring the world after Covid restrictions are lifted. First stop is either Iceland or Norway.

### My history

I graduated Second Division from BITS Pilani, Hyderabad Campus with a degree in Computer Science Engineering. In my final semester in college, I successfully defended my thesis on **Classification of Land Usage using Sentinel 2 Data**. I stayed in Bombay, Delhi, Vizag, Kochi and Goa in India and Watchfield in England.