---
title: Collection of Datasets
subtitle: A collection of Datasets ready for use
date: 2021-07-18 01:55:00
summary: 
    These are a collection of datasets which can be used to train a model. The dataset is split into training, validation and testing subsets in the ration 0.7:0.2:0.1. Either a CSV file contains the split or else the folders themselves are split into the subsets. Unless specified none of the datasets are mine.
tags: ["dataset", "model training"]
link_title: Datasets
link_project: https://gitlab.com/GotamDahiya/datasets
toc: true
weight: "4"
---

*Disclaimer*: Datasets with heading in red were curated and developed by me. In the other datasets, in case the credit is wrong please do bring it to my notice by commenting [here](https://gitlab.com/GotamDahiya/GotamDahiya.gitlab.io/-/issues/1) in the appropiate format.

There are a few datasets which are there in the repository but included here as they have not been maintained by me. 

## EuroSat

Link: [EuroSat(RGB)](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT)

This dataset is the RGB version of the full spectrum [Eurosat dataset](https://github.com/phelber/EuroSAT). Only the Blue[B02], Green[B03] and Red[B04] bands are present in each of the JPG images. Three CSVs are present which split the dataset into training, validation and testing maintaining the original distribution of images. The images are not present in EPSG:4326 format but rather in the regional EPSG format of the images. This maintains the spatial resolution to 10m exact. There are 10 classes,
1. [Annual Crop](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/AnnualCrop)
2. [Forest](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/Forest)
3. [Herbaceous Vegetation](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/HerbaceousVegetation)
4. [Highway](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/Highway)
5. [Industrial](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/Industrial)
6. [Pasture](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/Pasture)
7. [Permanent Crop](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/PermanentCrop)
8. [Residential](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/Residential)
9. [River](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/River)
10. [Sea & Lake](https://gitlab.com/GotamDahiya/datasets/-/tree/master/EuroSAT/SeaLake)

The original TIF images were converted to JPG using *gdal_translate*.

## Facial Expression Recognition Dataset

Link: [FER-Dataset](https://gitlab.com/GotamDahiya/datasets/-/tree/master/FER-Dataset)

This dataset contains the following facial expressions,
1. Anger
2. Fear
3. Happy
4. Neutral
5. Sad
6. Surprise

The dataset is not uniform and is skewed towards the *Angry* class. The model I developed could not differentiate between

## Image Search

### Image Search Version 1

### Image Search Version 2